
import React, { useEffect, useState } from 'react';
import { Col, InputNumber, Row, Slider, Space } from 'antd';
import { useDispatch } from 'react-redux';
import { handleLength } from '../store/settings/settingsSlice';

interface RangeInputProps {
  value: number | null;
  onChange?:any
}

export const RangeInput: React.FC<RangeInputProps> = () =>
{
  const dispatch=useDispatch()
  const [inputValue, setInputValue] = useState(26);
  useEffect(() =>
  {
    dispatch(handleLength(inputValue))
  },[inputValue])
  const onChange = (newValue: number) => {
    setInputValue(newValue);
  };

  return (
    <Row>
      <Col span={12}>
        <h5 style={{margin:'8px 0 30px 0',fontSize:'14px' }}>Character length:</h5>
        <Slider
          min={8}
          max={30}
          onChange={onChange}
          value={typeof inputValue === 'number' ? inputValue : 0}
        />
      </Col>
      <Col span={4}>
        <InputNumber
          min={8}
          max={30}
          style={{ margin: '0 16px'}}
          value={inputValue}
          onChange={onChange}
        />
      </Col>
    </Row>
  );
};
