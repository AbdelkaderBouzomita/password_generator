import { useState } from 'react';
import { FaArrowRight, FaRegCopy } from 'react-icons/fa';
import { useSelector } from 'react-redux';
import { RangeInput } from '../../components/RangeInput';
import { RootState } from '../../store/index';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { settingsState } from '../../store/settings/settingsSlice';
import {
  getRandomLowerLetters,
  getRandomUpperLetters,
  getRandomLetters,
  getRandomCharacters,
  getRandomAllCharacters,
  getRandomNumber,
  getRandomSymbol,
  getRandomNumberAndSymbol,
  getRandomLowerSymbolNumber,
  getRandomUpperSymbolNumber,
} from '../../helpers/functionrandom';

interface checkboxesType {
  upperCase: boolean;
  lowerCase: boolean;
  number: boolean;
  symbols: boolean;
}
const Home: React.FC = () => {
  const [checked, setChecked] = useState<checkboxesType>({
    upperCase: false,
    lowerCase: false,
    number: false,
    symbols: false,
  });
  const [value, setValue] = useState<number | null>(null);
  const [pwdValue, setPwdValue] = useState('P4$5W0rD!');
  const [strength, setStrength] = useState('medium');
  const { characterLength } = useSelector<RootState, settingsState>(
    (state) => state.settings
  );
  console.log(checked);
  const generatePwd = () => {
    if (
      !checked.lowerCase &&
      !checked.upperCase &&
      !checked.number &&
      !checked.symbols
    ) {
      toast.error('please check at least one');
    } else {
      if (
        checked.upperCase &&
        !checked.lowerCase &&
        !checked.number &&
        !checked.symbols
      ) {
        setPwdValue(getRandomUpperLetters(characterLength));

        {
          characterLength < 15 ? setStrength('slow') : setStrength('medium');
        }
      }
      if (
        !checked.upperCase &&
        checked.lowerCase &&
        !checked.number &&
        !checked.symbols
      ) {
        setPwdValue(getRandomLowerLetters(characterLength));
        {
          characterLength < 15 ? setStrength('slow') : setStrength('medium');
        }
      }
      if (
        !checked.upperCase &&
        !checked.lowerCase &&
        checked.number &&
        !checked.symbols
      ) {
        setPwdValue(getRandomNumber(characterLength));
        {
          characterLength < 15
            ? setStrength('slow')
            : characterLength < 20
            ? setStrength('medium')
            : setStrength('strong');
        }
      }
      if (
        !checked.upperCase &&
        !checked.lowerCase &&
        !checked.number &&
        checked.symbols
      ) {
        setPwdValue(getRandomSymbol(characterLength));
        characterLength < 15
          ? setStrength('slow')
          : characterLength < 20
          ? setStrength('medium')
          : setStrength('strong');
      }
      if (
        checked.upperCase &&
        checked.lowerCase &&
        !checked.number &&
        !checked.symbols
      ) {
        setPwdValue(getRandomLetters(characterLength));
        characterLength < 15
          ? setStrength('slow')
          : characterLength < 20
          ? setStrength('medium')
          : setStrength('strong');
      }
      if (
        checked.upperCase &&
        checked.lowerCase &&
        checked.number &&
        !checked.symbols
      ) {
        setPwdValue(getRandomCharacters(characterLength));
        characterLength < 9
          ? setStrength('slow')
          : characterLength < 14
          ? setStrength('medium')
          : characterLength < 23
          ? setStrength('strong')
          : setStrength('very strong');
      }
      if (
        checked.upperCase &&
        checked.lowerCase &&
        checked.number &&
        checked.symbols
      ) {
        setPwdValue(getRandomAllCharacters(characterLength));
        characterLength < 9
          ? setStrength('medium')
          : characterLength < 13
          ? setStrength('strong')
          : setStrength('very strong');
      }
      if (
        !checked.upperCase &&
        !checked.lowerCase &&
        checked.number &&
        checked.symbols
      ) {
        setPwdValue(getRandomNumberAndSymbol(characterLength));
      }
      if (
        !checked.upperCase &&
        checked.lowerCase &&
        checked.number &&
        checked.symbols
      ) {
        setPwdValue(getRandomLowerSymbolNumber(characterLength));
      }
      if (
        checked.upperCase &&
        !checked.lowerCase &&
        checked.number &&
        checked.symbols
      ) {
        setPwdValue(getRandomUpperSymbolNumber(characterLength));
      }
    }
  };
  const handleCopy = () => {
    navigator.clipboard.writeText(pwdValue);
    toast.success('Password copied');
  };
  return (
    <div className='password__generator__container'>
      <div className='password-generator'>
        <h4>Password Generator</h4>
        <div className='password__output'>
          <p placeholder='' style={{ fontSize: '11px' }}>
            {pwdValue}
          </p>
          <FaRegCopy
            style={{ cursor: 'pointer' }}
            onClick={() => handleCopy()}
          />
        </div>
        <div className='control__pannel'>
          <RangeInput value={value} onChange={setValue} />
          <div className='checkboxes'>
            <div>
              <input
                type='checkbox'
                name=''
                id=''
                onChange={() =>
                  setChecked((prevState) => ({
                    ...prevState,
                    upperCase: !prevState.upperCase,
                  }))
                }
              />
              <p>Include UpperCase Letters</p>
            </div>
            <div>
              <input
                type='checkbox'
                name=''
                id=''
                onChange={() =>
                  setChecked((prevState) => ({
                    ...prevState,
                    lowerCase: !prevState.lowerCase,
                  }))
                }
              />

              <p>Include LowerCase Letters</p>
            </div>
            <div>
              <input
                type='checkbox'
                name=''
                id=''
                onChange={() =>
                  setChecked((prevState) => ({
                    ...prevState,
                    number: !prevState.number,
                  }))
                }
              />

              <p>Include Numbers </p>
            </div>
            <div>
              <input
                type='checkbox'
                name=''
                id=''
                onChange={() =>
                  setChecked((prevState) => ({
                    ...prevState,
                    symbols: !prevState.symbols,
                  }))
                }
              />

              <p>Include Symbols</p>
            </div>
          </div>
          <div className='strengh-container'>
            <p>STRENGTH</p>
            <div className='strength-description'>
              <span>{strength}</span>
              <div className='level'>
                {strength === 'slow' ? (
                  <>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                  </>
                ) : strength === 'medium' ? (
                  <>
                    <nav></nav>
                    <nav></nav>
                    <div></div>
                    <div></div>
                  </>
                ) : strength === 'strong' ? (
                  <>
                    <nav></nav>
                    <nav></nav>
                    <nav></nav>
                    <div></div>
                  </>
                ) : (
                  <>
                    <nav></nav>
                    <nav></nav>
                    <nav></nav>
                    <nav></nav>
                  </>
                )}
              </div>
            </div>
          </div>
          <div className='generate--button' onClick={() => generatePwd()}>
            <span>GENERATE</span>
            <FaArrowRight />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;
