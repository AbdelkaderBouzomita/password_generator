export const getRandomUpperLetters = (count: number) => {
  let randomLetters = '';
  for (let i = 0; i < count; i++) {
    const randomCharCode = Math.floor(Math.random() * 26) + 97;
    const randomLetter = String.fromCharCode(randomCharCode);
    randomLetters += randomLetter;
  }
  return randomLetters.toUpperCase();
};
export const getRandomLowerLetters = (count: number) => {
  let randomLetters = '';
  for (let i = 0; i < count; i++) {
    const randomCharCode = Math.floor(Math.random() * 26) + 97;
    const randomLetter = String.fromCharCode(randomCharCode);
    randomLetters += randomLetter;
  }
  return randomLetters.toUpperCase().toLowerCase();
};

export const getRandomNumber = (count: number) => {
  let randomString = '';

  for (let i = 0; i < count; i++) {
    const randomNumber = Math.floor(Math.random() * 10);
    randomString += randomNumber.toString();
  }

  return randomString;
};

export const getRandomSymbol = (numSymbols: number) => {
  const symbols = '!@#$%^&*?><'; // List of symbols to choose from
  let randomSymbol = '';

  for (let i = 0; i < numSymbols; i++) {
    const randomIndex = Math.floor(Math.random() * symbols.length);
    randomSymbol += symbols.charAt(randomIndex);
  }

  return randomSymbol;
};
export const getRandomLetters = (count: number) => {
  let randomString = '';

  for (let i = 0; i < count; i++) {
    const randomCharCode = Math.floor(Math.random() * 26);
    const randomLetter = String.fromCharCode(randomCharCode + 97);
    const randomCase =
      Math.random() < 0.5
        ? randomLetter.toUpperCase()
        : randomLetter.toLowerCase();
    randomString += randomCase;
  }

  return randomString;
};

export const getRandomCharacters = (count: number) => {
  const uppercaseLetters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  const lowercaseLetters = 'abcdefghijklmnopqrstuvwxyz';
  const numbers = '0123456789';

  let randomString = '';
  randomString +=
    uppercaseLetters[Math.floor(Math.random() * uppercaseLetters.length)];
  randomString +=
    lowercaseLetters[Math.floor(Math.random() * lowercaseLetters.length)];

  // Generate at least one number
  randomString += numbers[Math.floor(Math.random() * numbers.length)];

  // Generate remaining characters
  for (let i = 3; i < count; i++) {
    const allCharacters = uppercaseLetters + lowercaseLetters + numbers;
    const randomIndex = Math.floor(Math.random() * allCharacters.length);
    randomString += allCharacters[randomIndex];
  }

  return randomString;
};

export const getRandomAllCharacters = (count: number) => {
  const uppercaseLetters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  const lowercaseLetters = 'abcdefghijklmnopqrstuvwxyz';
  const numbers = '0123456789';
  const symbols = '!@#$%^&*()_+-=[]{}|;:,.<>/?';

  let randomString = '';

  // Generate at least one uppercase letter
  randomString +=
    uppercaseLetters[Math.floor(Math.random() * uppercaseLetters.length)];

  // Generate at least one lowercase letter
  randomString +=
    lowercaseLetters[Math.floor(Math.random() * lowercaseLetters.length)];

  // Generate at least one number
  randomString += numbers[Math.floor(Math.random() * numbers.length)];

  // Generate at least one symbol
  randomString += symbols[Math.floor(Math.random() * symbols.length)];

  // Generate remaining characters
  for (let i = 4; i < count; i++) {
    const allCharacters =
      uppercaseLetters + lowercaseLetters + numbers + symbols;
    const randomIndex = Math.floor(Math.random() * allCharacters.length);
    randomString += allCharacters[randomIndex];
  }

  return randomString;
};
export const getRandomUpperSymbolNumber = (count: number) => {
  const uppercaseLetters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

  const numbers = '0123456789';
  const symbols = '!@#$%^&*()_+-=[]{}|;:,.<>/?';

  let randomString = '';

  // Generate at least one uppercase letter
  randomString +=
    uppercaseLetters[Math.floor(Math.random() * uppercaseLetters.length)];



  // Generate at least one number
  randomString += numbers[Math.floor(Math.random() * numbers.length)];

  // Generate at least one symbol
  randomString += symbols[Math.floor(Math.random() * symbols.length)];

  // Generate remaining characters
  for (let i = 4; i < count; i++) {
    const allCharacters =
      uppercaseLetters + numbers + symbols;
    const randomIndex = Math.floor(Math.random() * allCharacters.length);
    randomString += allCharacters[randomIndex];
  }

  return randomString;
};
export const getRandomLowerSymbolNumber = (count: number) => {

  const lowercaseLetters = 'abcdefghijklmnopqrstuvwxyz';
  const numbers = '0123456789';
  const symbols = '!@#$%^&*()_+-=[]{}|;:,.<>/?';

  let randomString = '';

  // Generate at least one uppercase letter


  // Generate at least one lowercase letter
  randomString +=
    lowercaseLetters[Math.floor(Math.random() * lowercaseLetters.length)];

  // Generate at least one number
  randomString += numbers[Math.floor(Math.random() * numbers.length)];

  // Generate at least one symbol
  randomString += symbols[Math.floor(Math.random() * symbols.length)];

  // Generate remaining characters
  for (let i = 4; i < count; i++) {
    const allCharacters =
      lowercaseLetters + numbers + symbols;
    const randomIndex = Math.floor(Math.random() * allCharacters.length);
    randomString += allCharacters[randomIndex];
  }

  return randomString;
};
export const getRandomNumberAndSymbol = (count: number) => {
  const numbers = '0123456789';
  const symbols = '!@#$%^&*()_+-=[]{}|;:,.<>/?';

  let randomString = '';
  // Generate at least one number
  randomString += numbers[Math.floor(Math.random() * numbers.length)];

  // Generate at least one symbol
  randomString += symbols[Math.floor(Math.random() * symbols.length)];

  // Generate remaining characters
  for (let i = 4; i < count; i++) {
    const allCharacters = numbers + symbols;
    const randomIndex = Math.floor(Math.random() * allCharacters.length);
    randomString += allCharacters[randomIndex];
  }

  return randomString;
};

