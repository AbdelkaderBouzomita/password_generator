import { createSlice, current } from "@reduxjs/toolkit";

export interface settingsState {
  characterLength: number;
}

const initialState: settingsState = {
  characterLength: 26,

};

const settingsSlice = createSlice({
  name: 'settings',
  initialState,
  reducers: {
    handleLength: (state,action) => {
      state.characterLength = action.payload;
    },

  },
});

export const {
  handleLength,

} = settingsSlice.actions;

export default settingsSlice.reducer;
